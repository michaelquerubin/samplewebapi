﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using Newtonsoft.Json;
using PracticeWebAPI.Models;
using PracticeWebAPI.Models.ViewModel;

namespace PracticeWebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [System.Web.Http.Route("api/GetUserInformation")]
        [System.Web.Http.HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public JsonResult GetUserInformation(int unum, string upass)
        {
            var response = new JsonResult();
            TestDBEntities TestDBEntities = new TestDBEntities();
            bool UserInfoResult = false;

            try
            {
                var loginData = TestDBEntities.GetUserInfo(unum, upass).SingleOrDefault();
                if (loginData != null)
                {
                    response.Data = new
                    {
                        UserInfoResult = true,
                        UserID = loginData.ID,
                        UserCIM = loginData.CIM,
                        UserName = loginData.Fullname,
                        UserEmail = loginData.Email,
                        UserAddress = loginData.Address

                    };
                }
                else
                {
                    response.Data = new
                    {
                        UserInfoResult = false
                    };

                }
            }
            catch (Exception ex)
            {
                response.Data = new
                {
                    UserInfoResult = false
                };
            }
            return response;
        }

        [System.Web.Http.Route("api/InsertPost")]
        [System.Web.Http.HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public JsonResult InsertPost(int UserID, string Post)
        {
            var response = new JsonResult();
            TestDBEntities TestDBEntities = new TestDBEntities();

            try
            {
                TestDBEntities.InsertPost(UserID, Post);

                response.Data = new
                {
                    UserInfoResult = true

                };
            }
            catch (Exception ex)
            {
                response.Data = new
                {
                    UserInfoResult = false
                };
            }
            return response;
        }

        [System.Web.Http.Route("api/GetUserPost")]
        [System.Web.Http.HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public JsonResult GetUserPost()
        {
            var response = new JsonResult();
            TestDBEntities TestDBEntities = new TestDBEntities();
            List<GetUserPost> PostData = new List<GetUserPost>();
            try
            {
                PostData = TestDBEntities.GetUserPostList().Select(x => new GetUserPost
                {
                    PostID = x.ID,
                    UserID = x.UserID,
                    Fullname = x.Fullname,
                    Post = x.Post,
                    PostDate = x.PostDatetime.ToString()
                }).ToList();

                var PostUserList = JsonConvert.SerializeObject(PostData);

                response.Data = new
                {
                    PostUserList
                };

                return response;


            }
            catch(Exception ex)
            {

            }
            return response;
        }
    }
}
