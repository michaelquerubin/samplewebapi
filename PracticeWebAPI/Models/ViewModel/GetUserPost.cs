﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeWebAPI.Models.ViewModel
{
    public class GetUserPost
    {
        public int? PostID { get; set; }
        public int? UserID { get; set; }
        public string Fullname { get; set; }
        public string Post { get; set; }
        public string PostDate { get; set; }
    }
    public class GetUserPostList : List<GetUserPost> { };
}